(function ($) {
  /* navbar */
  
  $(document).ready(function() {
    // run test on initial page load
    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize);
  });
  
  $('.nav-list').click( 
    function(){
      if ($(this).find('div.drop-menu').hasClass('sr-only'))
        {showDrop(this)}
      else
        {hideDrop(this)}
    }
  )
    
  function checkSize(){
    console.log($('nav#navbar').css('flex-flow'));
    if ($('nav#navbar').css('flex-flow') == 'row wrap'){
      $('.nav-list').hover(function(){
        showDrop(this);
        setDropCSS(this);
      },function(){hideDrop(this)});
    } else {
      $('.nav-list').unbind('mouseenter mouseleave');
    }
  }  
  
  function hideDrop(dropMenuParent) {
    $(dropMenuParent).find('div.drop-menu').addClass('sr-only').removeClass('showDrop');
  }
  
  function showDrop(dropMenuParent) {
    $(dropMenuParent).find('div.drop-menu').removeClass('sr-only').addClass('showDrop');
  }
  
  function setDropCSS(dropMenuParent) {
    var position = $('nav#navbar').position();
    var positionTop = position.top;
    var navMarginLeft = $('nav#navbar').css("margin-left");
    var positionLeft = parseInt(navMarginLeft) + 15.5;
    positionLeft = positionLeft.toString() + "px";
    var navHeight = $('nav#navbar').outerHeight();
    var navWidth = parseInt($('nav#navbar').outerWidth()) - 30;
    navWidth = navWidth.toString() + "px";
    var dropPostionTop = positionTop + navHeight - 10;
    $(dropMenuParent).find('div.drop-menu').css({"top": dropPostionTop, "left": positionLeft, "width": navWidth});
  }
    
    
    
})(jQuery);