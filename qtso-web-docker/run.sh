psql -h localhost -p 5432 -U postgres -d postgres -c "DROP DATABASE drupaldb";
psql -h localhost -p 5432 -U postgres -d postgres -c "CREATE DATABASE drupaldb";
psql -h localhost -p 5432 -U postgres -d drupaldb -c "DROP USER drupaluser";

psql -h localhost -p 5432 -U postgres -d drupaldb -c "CREATE USER drupaluser WITH SUPERUSER PASSWORD 'drupalpassword'";

pg_restore -h localhost -p 5432 -U postgres -d drupaldb -v qtso-db-backups