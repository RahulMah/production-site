# qtso-dev and qtso-web-docker should be under QTSO_FOLDER Path
QTSO_FOLDER="/Users/r.maheshuni/Desktop/production-site"

dos2unix refresh-drupal.sh
dos2unix run.sh
dos2unix qtso-web-deploy.sh

### echo to users these options
# 1) fresh start from scratch
# 2) Update just DB
docker volume create --name postgresql-data -d local

docker run -d -p 5434:5432 -v postgresql-data:/var/lib/postgresql/data  -v $QTSO_FOLDER/qtso-web-docker/run.sh:/run.sh  --name qtsowebdocker_qtso-db_1 prabhushan/qtso-db:1.2.5

echo $QTSO_FOLDER
#docker build . -t qtsowebdocker_qtso-web

docker run -p 8282:80 --name qtsowebdocker_qtso-web_1 -v $QTSO_FOLDER/qtso-dev:/var/www/html -v $QTSO_FOLDER/qtso-web-docker/settings.php:/var/www/html/sites/default/settings.php -v $QTSO_FOLDER/qtso-web-docker/logs:/var/log/httpd --link qtsowebdocker_qtso-db_1 -d prabhushan/qtsowebdocker_qtso-web:1.2.1 

docker cp qtso-db-backups qtsowebdocker_qtso-db_1:/
docker exec -it qtsowebdocker_qtso-db_1 bash ./run.sh

docker cp refresh-drupal.sh qtsowebdocker_qtso-web_1:/
docker exec -it qtsowebdocker_qtso-web_1 bash refresh-drupal.sh


# UPDATE DB
# get latest back up from AWS or bitbucket and place in qtso-web-docker folder
# docker cp qtso-db-backups qtsowebdocker_qtso-db_1:/
# docker exec -it qtsowebdocker_qtso-db_1 /bin/bash ./run.sh


# Just refresh Drupal 

#  docker rm -f qtsowebdocker_qtso-web_1
#  docker run -p 8181:80 --name qtsowebdocker_qtso-web_1 -v $QTSO_FOLDER/qtso-dev:/var/www/html -v $QTSO_FOLDER/qtso-web-docker/settings.php:/var/www/html/sites/default/settings.php -v $QTSO_FOLDER/qtso-web-docker/logs:/var/log/httpd --link qtsowebdocker_qtso-db_1 -d prabhushan/qtsowebdocker_qtso-web:1.2.0 
#  docker cp refresh-drupal.sh qtsowebdocker_qtso-web_1:/
#  docker exec -it qtsowebdocker_qtso-web_1 /bin/bash refresh-drupal.sh


# # System restart - just restart docker from where it is left off (no new DB)
# docker run -d -p 5432:5432 -v postgresql-data:/var/lib/postgresql/data --name qtsowebdocker_qtso-db_1 prabhushan/qtso-db:1.2.4
# docker run -p 8181:80 --name qtsowebdocker_qtso-web_1 -v $QTSO_FOLDER/qtso-dev:/var/www/html -v $QTSO_FOLDER/qtso-web-docker/settings.php:/var/www/html/sites/default/settings.php -v $QTSO_FOLDER/qtso-web-docker/logs:/var/log/httpd --link qtsowebdocker_qtso-db_1 -d prabhushan/qtsowebdocker_qtso-web:1.2.0 
# docker cp refresh-drupal.sh qtsowebdocker_qtso-web_1:/
# docker exec -it qtsowebdocker_qtso-web_1 /bin/bash refresh-drupal.sh
